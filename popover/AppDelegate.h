//
//  AppDelegate.h
//  popover
//
//  Created by Daniel Phillips on 14/05/2013.
//  Copyright (c) 2013 djp. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
