//
//  ViewController.h
//  popover
//
//  Created by Daniel Phillips on 14/05/2013.
//  Copyright (c) 2013 djp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *clearButton;
- (IBAction)showPressed:(id)sender;

- (IBAction)clearPressed:(id)sender;
@end
