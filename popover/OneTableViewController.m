//
//  OneTableViewController.m
//  popover
//
//  Created by Daniel Phillips on 14/05/2013.
//  Copyright (c) 2013 djp. All rights reserved.
//

#import "OneTableViewController.h"

#import "TwoTableViewController.h"

@implementation OneTableViewController


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.title = @"{350.0, 350.0}";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGSize size = CGSizeMake(350.0, 350.0); // size of view in popover
    self.contentSizeForViewInPopover = size;

    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    cell.textLabel.text = @"Square Table View";
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TwoTableViewController *two = [[TwoTableViewController alloc] init];
    
    [self.navigationController pushViewController:two animated:YES];
}

@end
