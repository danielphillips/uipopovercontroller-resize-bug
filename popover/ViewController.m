//
//  ViewController.m
//  popover
//
//  Created by Daniel Phillips on 14/05/2013.
//  Copyright (c) 2013 djp. All rights reserved.
//

#import "ViewController.h"
#import "OneTableViewController.h"

@interface ViewController ()
@property (strong) UIPopoverController *popover;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.clearButton.enabled = NO;
}

- (IBAction)showPressed:(UIButton *)sender {
    if( !self.popover ){
        OneTableViewController *one = [[OneTableViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:one];
        self.popover = [[UIPopoverController alloc] initWithContentViewController:nav];
        
        self.clearButton.enabled = YES;
    }
    
    [self.popover presentPopoverFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
    
}

- (IBAction)clearPressed:(id)sender {
    self.popover = nil;
    
    self.clearButton.enabled = NO;
}

@end
